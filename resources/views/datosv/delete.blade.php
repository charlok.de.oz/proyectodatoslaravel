<form action="{{url('/datos/'.$dato->id)}}" method="POST" class="d-inline-block">
    @csrf
    {{method_field('DELETE')}}

    <input type="submit" class="btn btn-outline-danger" value="Eliminar">
</form>