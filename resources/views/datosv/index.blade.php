@extends('layouts.app')

@section('content')

<div class="text-center">
    <h1>Datos:</h1>
</div>
<div class="container">
    <table class="table table-hover">
    <thead class="thead-ligth">
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Fecha de Nacimiento</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($datos as $dato)
        <tr>
            <td>{{$dato->id}}</td>
            <td>{{$dato->Nombre}}</td>
            <td>{{$dato->ApellidoPat}}</td>
            <td>{{$dato->ApellidoMat}}</td>
            <td>{{$dato->FechaDeNacimiento}}</td>
            <td>
            <a href="{{url('/datos/'.$dato->id.'/edit')}}" class="btn btn-outline-warning">Editar</a>
            @include('datosv.delete',['$dato'=>$dato])
            </td>
        </tr>
        @endforeach()
    </tbody>
    </table>
    <a href="{{url('/datos/create')}}" class="btn btn-outline-primary">Agregar</a>
</div>

@endsection