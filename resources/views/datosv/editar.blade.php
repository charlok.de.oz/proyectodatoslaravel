@extends('layouts.app')

@section('content')

<form action="{{route('datos.update',$dato->id)}}" method="POST">
        {{method_field('PATCH')}}
        @csrf 
        <div class="container">
            <div class="form-group">
                <input type="text" class="form-control" name="nombre" value="{{$dato->Nombre}}" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="apellidoPat" value="{{$dato->ApellidoPat}}" placeholder="Apellido Paterno">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="apellidoMat" value="{{$dato->ApellidoMat}}" placeholder="Apellido Materno">
            </div>
            <div class="form-group">
                <input type="date" class="form-control" name="fechaDeNacimiento" value="{{$dato->FechaDeNacimiento}}">
            </div>
            
            
            <button type="submit" class="btn btn-outline-success">Enviar</button>
    
    </div>
        </form>
@endsection