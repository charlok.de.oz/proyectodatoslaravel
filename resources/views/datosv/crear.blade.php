@extends('layouts.app')

@section('content')

        <form action="{{route('datos.store')}}" method="POST">
        @csrf 
        <div class="container">
            <div class="form-group">
                <input type="text" class="form-control" name="nombre" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="apellidoPat" placeholder="Apellido Paterno">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="apellidoMat" placeholder="Apellido Materno">
            </div>
            <div class="form-group">
                <input type="date" class="form-control" name="fechaDeNacimiento">
            </div>
            
            
            <button type="submit" class="btn btn-outline-success">Enviar</button>
    
    </div>
        </form>
@endsection